## Perintah Git ##

1. Membuat repository local

    ```
    git init
    ```

2. Melihat kondisi repo local

    ```
    git status
    ```

3. Menambahkan file ke staging area

    ```
    git add namafile
    ```

4. Menyimpan isi staging area ke local repo

    ```
    git commit -m "keterangan perubahan"
    ```

5. Mendaftarkan remote repository

    ```
    git remote add <nama-alias-repo> <url-repo>
    ```

    Contoh :

    ```
    git remote add gitlab git@gitlab.com:training-devops-2023-01/materi-devops.git
    ```

6. Mengupload isi branch `main` di local repo ke branch `main` di remote repo

    ```
    git push gitlab main
    ```

7. Membuat branch

    ```
    git branch <nama-branch> <commit-id-tempat-mulai-branchnya>
    ```

    Contoh

    ```
    git branch fix-url-latihan f057570
    ```

8. Pindah/masuk ke branch `fix-url-latihan`

    ```
    git checkout fix-url-latihan
    ```

9. Melihat grafik history branch

    ```
    git log --oneline --all --graph
    ```

10. Push branch ke remote repository

    ```
    git push <nama-remote> <nama-branch>
    ```

    Contoh :

    ```
    git push gitlab fix-url-latihan
    ```

12. Delete branch di remote repository

    * Push branch kosong / null ke remote

        ```
        git push gitlab :fix-url-latihan
        ```

11. Menggabungkan branch `fix-url-latihan` ke dalam branch `main`

    ```
    git checkout main
    git merge fix-url-latihan
    ```

12. Simulasi konflik. Konflik terjadi karena ada pengeditan di baris yang sama. Berikut adalah langkah-langkah untuk mensimulasikan terjadinya konflik

    * Buat branch lain

        ```
        git branch simulasi-konflik
        ```

    * Lakukan edit dan commit di file yang sama di masing-masing branch

    * Lakukan merge

    * Save dan commit hasilnya
